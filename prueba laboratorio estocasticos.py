import numpy as np
import csv
from tkinter import *
datos = 'C:\\Users\\BRIAN\\Documents\\Python\\Estocasticos\\BD-Loteria.csv'
num = []
with open(datos,encoding="latin-1") as csvfile:
    reader = csv.reader(csvfile, delimiter=",")
    for row in reader:
        num.append(int(row[1]))
    base_datos=np.array(num)
    #print (type(base_datos))
    print("Numero total de resultados: "+str(len(num)))
    
def busc(bus1, bus2, datos):
    max = len(datos)
    i = 0
    cont = 0
    while i < max:
        if datos[i] == bus1:
            if i < max - 1:
                if datos[i + 1] == bus2:
                    cont += 1
        i += 1
    contTotal=0

    for k in datos:
        if k==bus1:
            contTotal+=1

    return cont/contTotal

def rev(par, d1, d2):
    max = len(par)
    i = 0
    par.append([d1,d2])
    while i < max:
        if par[i] == [d1,d2]:
            return False
        i += 1

    return True

def master():
    datos = base_datos
    par = []
    max = len(datos) - 1
    i = 0
    
    matrixResult = np.zeros((1000, 1000 ))

    while i < max:
        d1 = int(datos[i])
        d2 = int(datos[i+1])
        if rev(par, d1, d2):
            columna=int(datos[i])
            fila=int(datos[i+1])
            probabilidad=busc(d1, d2, datos)
            matrixResult[columna][fila] = probabilidad
            #x.append(probabilidad)

        i += 1
    #print(matrixResult[0][0])
    
    return matrixResult;

matriz_estocastica=master()

def pden(n,matriz,ganador):
    transpuesta=matriz.transpose()
    dt_in=np.zeros(1000)
    dt_in[ganador]=1
    contador=0
    probabilidad=dt_in.copy()
    for i in range (n):
        probabilidad=dt_in.copy()
        dt_in=np.dot(transpuesta,dt_in)    #Multiplicaciòn de matriz transpuesta
    #print ("contado2r = "+ str(contador))
    return dt_in

def calcular_de_un_numero(n,matriz,ganador,supuesto):
    transpuesta=matriz.transpose()
    dt_in=np.zeros(1000)
    dt_in[ganador]=1
    
    contador=0
    probabilidad=dt_in.copy()
    for i in range (n):
        probabilidad=dt_in.copy()
        dt_in=np.dot(transpuesta,dt_in)
    
    print("La probabilidad que gane "+str(supuesto)+" es del "+str(dt_in[supuesto]))
    
dias=int(input("Digite a los cuantos dias desea conocer el o los posibles numeros ganadores: "))
numeroactual=int(input("Digite el numero ganador del dia de ayer: "))
#dias=1
#numeroactual=122

resultado =pden(dias,matriz_estocastica,numeroactual)

maxima_probabilidad=np.amax(resultado)

print("Los posibles numeros ganadores son: ")
for i in range(len(resultado)):
    #print("probabilidad de "+str(i) +" sea el numero ganador = "+str(resultado[i]))
    #print(i)
    if resultado[i]==maxima_probabilidad:
        if  i<100 :
            print(str(i)+"0")
        else:
            print(i)
suma=0
for i in resultado:
    suma+=i
print("propablidad total: "+str(suma))
print("teniendo una probabilidad de :"+str(maxima_probabilidad))



dias=int(input("en cuantos dias quiere saber el numero: "))
numeroactual=int(input("cual es el numero ganador de ayer?: "))
supuesto=int(input("cual es el numero que quiere probar?: "))
#dias=1
#numeroactual=122
#supuesto=970
calcular_de_un_numero(dias,matriz_estocastica,numeroactual,supuesto)

#Interfaz gráfica
ventana = Tk()
ventana.title("Loteria")
ventana.geometry("400x200")
ventana.mainloop()
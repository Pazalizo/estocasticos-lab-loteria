import csv
import random

# Abre el archivo CSV original y crea un archivo de salida
with open('C:\\Users\\huert\\OneDrive\\Documentos\\Yo\\Yo\\Python Aprender\\Estocasticos Lab de Loteria\\BD-Loteria.csv', 'r') as archivo_original, open('C:\\Users\\huert\\OneDrive\\Documentos\\Yo\\Yo\\Python Aprender\\Estocasticos Lab de Loteria\\BD-Loteria-Modificada.csv', 'w', newline='') as archivo_modificado:
    lector_csv = csv.reader(archivo_original)
    escritor_csv = csv.writer(archivo_modificado)

    for fila in lector_csv:
        if len(fila) >= 3:
            fecha = fila[0]  # Primer campo (fecha)
            segundo_campo = fila[1]  # Segundo campo
            parte_anterior = segundo_campo[:-3]  # Elimina los últimos 3 dígitos
            numeros_aleatorios = str(random.randint(0, 999)).zfill(3)  # Genera números aleatorios
            nuevo_segundo_campo = parte_anterior + numeros_aleatorios  # Combina la parte anterior con los números aleatorios
            fila[1] = nuevo_segundo_campo  # Actualiza el segundo campo
            escritor_csv.writerow(fila)  # Escribe la fila modificada
        else:
            escritor_csv.writerow(fila)

print("Proceso completado. El archivo modificado se ha guardado como 'archivo_modificado.csv'.")


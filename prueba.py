import tkinter as tk
from tkinter import messagebox
import csv
import numpy as np

class LotteryApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Lottery App")

        # Etiquetas
        self.label_dias = tk.Label(self, text="Número de días:")
        self.label_numero_actual = tk.Label(self, text="Número ganador anterior:")

        # Entradas de texto
        self.entry_dias = tk.Entry(self)
        self.entry_numero_actual = tk.Entry(self)

        # Botones para los modos
        self.button_probabilidad_alta = tk.Button(self, text="Calcular Probabilidad Alta", command=self.calcular_probabilidad_alta)
        self.button_probabilidad_numero = tk.Button(self, text="Calcular Probabilidad de Número", command=self.calcular_probabilidad_numero)

        # Posicionamiento de los componentes en la ventana
        self.label_dias.pack()
        self.entry_dias.pack()
        self.label_numero_actual.pack()
        self.entry_numero_actual.pack()
        self.button_probabilidad_alta.pack()
        self.button_probabilidad_numero.pack()

    def cargar_datos(self, ruta_archivo):
        num = []
        with open(ruta_archivo, encoding="latin-1") as csvfile:
            reader = csv.reader(csvfile, delimiter=",")
            for row in reader:
                num.append(int(row[1]))
        return np.array(num)

    def calcular_probabilidad_alta(self):
        datos = self.cargar_datos('C:\\Users\\huert\\OneDrive\\Documentos\\Yo\\Yo\\Python Aprender\\Estocasticos Lab de Loteria\\BD-Loteria.csv')
        dias = int(self.entry_dias.get())
        numero_actual = int(self.entry_numero_actual.get())

        def rev(par, d1, d2):
            max = len(par)
            i = 0
            par.append([d1, d2])
            while i < max:
                if par[i] == [d1, d2]:
                    return False
                i += 1
            return True

        def master():
            par = []
            max = len(datos) - 1
            i = 0

            matrixResult = np.zeros((1000, 1000))

            while i < max:
                d1 = int(datos[i])
                d2 = int(datos[i + 1])
                if rev(par, d1, d2):
                    columna = int(datos[i])
                    fila = int(datos[i + 1])
                    probabilidad = self.busc(d1, d2, datos)
                    matrixResult[columna][fila] = probabilidad
                i += 1
            return matrixResult

        matriz_estocastica = master()

        def pden(n, matriz, ganador):
            transpuesta = matriz.transpose()
            dt_in = np.zeros(1000)
            dt_in[ganador] = 1
            contador = 0
            probabilidad = dt_in.copy()
            for i in range(n):
                probabilidad = dt_in.copy()
                dt_in = np.dot(transpuesta, dt_in)
            return dt_in

        resultado = pden(dias, matriz_estocastica, numero_actual)
        maxima_probabilidad = np.amax(resultado)

        numeros_ganadores = []
        for i in range(len(resultado)):
            if resultado[i] == maxima_probabilidad:
                if i < 100:
                    numeros_ganadores.append(str(i) + "0")
                else:
                    numeros_ganadores.append(str(i))

        suma = np.sum(resultado)
        messagebox.showinfo("Probabilidad Alta", f"Los posibles números ganadores son: {', '.join(numeros_ganadores)}\n\n"
                                               f"Probabilidad total: {suma}\n"
                                               f"Teniendo una probabilidad de: {maxima_probabilidad}")

    def calcular_probabilidad_numero(self):
        datos = self.cargar_datos('C:\\Users\\huert\\OneDrive\\Documentos\\Yo\\Yo\\Python Aprender\\Estocasticos Lab de Loteria\\BD-Loteria.csv')
        dias = int(self.entry_dias.get())
        numero_actual = int(self.entry_numero_actual.get())
        numero_supuesto = int(input("Ingresa el número supuesto:"))

        def rev(par, d1, d2):
            max = len(par)
            i = 0
            par.append([d1, d2])
            while i < max:
                if par[i] == [d1, d2]:
                    return False
                i += 1
            return True

        def master():
            par = []
            max = len(datos) - 1
            i = 0

            matrixResult = np.zeros((1000, 1000))

            while i < max:
                d1 = int(datos[i])
                d2 = int(datos[i + 1])
                if rev(par, d1, d2):
                    columna = int(datos[i])
                    fila = int(datos[i + 1])
                    probabilidad = self.busc(d1, d2, datos)
                    matrixResult[columna][fila] = probabilidad
                i += 1
            return matrixResult

        matriz_estocastica = master()

        def pden(n, matriz, ganador):
            transpuesta = matriz.transpose()
            dt_in = np.zeros(1000)
            dt_in[ganador] = 1
            contador = 0
            probabilidad = dt_in.copy()
            for i in range(n):
                probabilidad = dt_in.copy()
                dt_in = np.dot(transpuesta, dt_in)
            return dt_in

        resultado = pden(dias, matriz_estocastica, numero_actual)

        messagebox.showinfo("Probabilidad de Número", f"La probabilidad que gane {numero_supuesto} es del {resultado[numero_supuesto]}")

if __name__ == "__main__":
    app = LotteryApp()
    app.mainloop()

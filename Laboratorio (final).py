import tkinter as tk
from tkinter import messagebox
import csv
import numpy as np

class LotteryApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Lottery App")

        # Etiquetas
        self.label_dias = tk.Label(self, text="Número de días:") #Para pedir el numero de dias
        self.label_numero_actual = tk.Label(self, text="Número ganador anterior:")
        self.label_supuesto = tk.Label(self, text="Número supuesto:")

        # Entradas de texto
        self.entry_dias = tk.Entry(self)
        self.entry_numero_actual = tk.Entry(self)
        self.entry_supuesto = tk.Entry(self)

        # Botón de cálculo
        self.button_calcular = tk.Button(self, text="Calcular", command=self.calcular_numeros_ganadores)

        # Posicionamiento de los componentes en la ventana
        self.label_dias.pack()
        self.entry_dias.pack()
        self.label_numero_actual.pack()
        self.entry_numero_actual.pack()
        self.label_supuesto.pack()
        self.entry_supuesto.pack()
        self.button_calcular.pack()

    def cargar_datos(self, ruta_archivo):
        num = []
        with open(ruta_archivo, encoding="latin-1") as csvfile:
            reader = csv.reader(csvfile, delimiter=",")
            for row in reader:
                num.append(int(row[1]))
        return np.array(num)

    def busc(self, bus1, bus2, datos):
        max = len(datos)
        i = 0
        cont = 0
        while i < max:
            if datos[i] == bus1:
                if i < max - 1:
                    if datos[i + 1] == bus2:
                        cont += 1
            i += 1
        contTotal = 0

        for k in datos:
            if k == bus1:
                contTotal += 1

        return cont / contTotal

    def calcular_numeros_ganadores(self):
        datos = self.cargar_datos('C:\\Users\\huert\\OneDrive\\Documentos\\Yo\\Yo\\Python Aprender\\Estocasticos Lab de Loteria\\BD-Loteria-Chonquito.csv')
        dias = int(self.entry_dias.get())
        numero_actual = int(self.entry_numero_actual.get())
        supuesto = int(self.entry_supuesto.get())

        def rev(par, d1, d2):
            max = len(par)
            i = 0
            par.append([d1, d2])
            while i < max:
                if par[i] == [d1, d2]:
                    return False
                i += 1
            return True

        def master():
            par = []
            max = len(datos) - 1
            i = 0

            matrixResult = np.zeros((1000, 1000))

            while i < max:
                d1 = int(datos[i])
                d2 = int(datos[i + 1])
                if rev(par, d1, d2):
                    columna = int(datos[i])
                    fila = int(datos[i + 1])
                    probabilidad = self.busc(d1, d2, datos)
                    matrixResult[columna][fila] = probabilidad
                i += 1
            return matrixResult

        matriz_estocastica = master()

        def pden(n, matriz, ganador):
            transpuesta = matriz.transpose()
            dt_in = np.zeros(1000)
            dt_in[ganador] = 1
            contador = 0
            probabilidad = dt_in.copy()
            for i in range(n):
                probabilidad = dt_in.copy()
                dt_in = np.dot(transpuesta, dt_in)
            return dt_in

        def calcular_de_un_numero(n, matriz, ganador, supuesto):
            transpuesta = matriz.transpose()
            dt_in = np.zeros(1000)
            dt_in[ganador] = 1
            contador = 0
            probabilidad = dt_in.copy()
            for i in range(n):
                probabilidad = dt_in.copy()
                dt_in = np.dot(transpuesta, dt_in)

            messagebox.showinfo("Probabilidad", f"La probabilidad que gane {supuesto} es del {dt_in[supuesto]}")

        resultado = pden(dias, matriz_estocastica, numero_actual)
        maxima_probabilidad = np.amax(resultado)

        numeros_ganadores = []
        for i in range(len(resultado)):
            if resultado[i] == maxima_probabilidad:
                if i < 100:
                    numeros_ganadores.append(str(i) + "0")
                else:
                    numeros_ganadores.append(str(i))

        suma = np.sum(resultado) 
        messagebox.showinfo("Resultados", f"Los posibles números ganadores son: {', '.join(numeros_ganadores)}\n\n"
                                           f"Teniendo una probabilidad de: {maxima_probabilidad}")

        dias = int(self.entry_dias.get())
        numero_actual = int(self.entry_numero_actual.get())
        supuesto = int(self.entry_supuesto.get())
        calcular_de_un_numero(dias, matriz_estocastica, numero_actual, supuesto)

if __name__ == "__main__":
    app = LotteryApp()
    app.mainloop()
